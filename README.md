Configuration for a local Vaultwarden setup
===========================================

This repository contains a docker compose file for a local vaultwarden setup. Variables used in the docker compose file need to be provided in an .env file.
